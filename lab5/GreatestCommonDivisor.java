import java.util.Scanner;

public class GreatestCommonDivisor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("First Number =");
        int a = scanner.nextInt();
        System.out.print("Second Number =");
        int b = scanner.nextInt();

        int greater = a > b ? a:b ;
        int smaller = a < b ? a:b ;

        int q = greater / smaller ;
        int  r = greater % smaller ;

        do{
            System.out.println("Greater =" + greater);
            System.out.println("Smaller =" + smaller);
            q = greater / smaller;
            r = greater % smaller;
            System.out.println("Remainder = " + r);
            greater = smaller ;
            smaller = r;

        }while(r != 0);

        System.out.println("GDC = " + greater);
    }
}
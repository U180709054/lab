import java.util.Scanner;

public class GDCRec {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("First Number =");
        int a = scanner.nextInt();
        System.out.print("Second Number =");
        int b = scanner.nextInt();
        int greater = a > b ? a : b;
        int smaller = a < b ? a : b;
        System.out.println("GCD = " + gdc(greater, smaller));

    }


    public static int gdc(int greater, int smaller) {
        if (smaller == 0)
            return greater;
        return gdc(smaller, greater % smaller);


    }
}

import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int player=0;
		char a;
		printBoard(board);
		boolean finished= false;
		game:for (int i =0;i<9;i++) {
			if (i%2==0)
			{
				player=1;
				a='X';
			}
			else {
				player=2;
				a='O';
			}
			System.out.print("Player "+ player +" enter row number:");
			int row = reader.nextInt();
			if (row>3 || row <=0)
			{
				System.out.println("Please enter a number between 1 and 3!");
				i--;
				continue;

			}
			System.out.print("Player "+player+" enter column number:");
			int col = reader.nextInt();
			if (col>3 || col <=0)
			{
				System.out.println("Please enter a number between 1 and 3!");
				i--;
				continue;

			}
			if (board[row -1][col -1]==' ')
				board[row - 1][col - 1] = a;
			else {
				System.out.println("This slot already taken!");
				i--;
				continue;
			}
			printBoard(board);
			finished= checkBoard(board,player);
			if (finished==true)
				break game;

		}
		if(finished== false)
			System.out.println("Draw!");

		reader.close();
	}
	public static boolean checkBoard(char[][] board,int player){
		for (int j=0,h=2;j<3;j++,h--)
		{

			if ((board[j][0]==board[j][1])&&(board[j][1]==board[j][2])&&(board[j][1]!=' '))
			{
				System.out.println("Player "+player +" Wins!");
				return true;

			}
			if((board[0][0]==board[1][1])&&(board[1][1]==board[2][2])&&(board[1][1]!=' '))
			{
				System.out.println("Player "+player +" Wins!");
				return true;
			}
			if((board[0][2]==board[1][1])&&(board[1][1]==board[2][0])&&(board[1][1]!=' '))
			{
				System.out.println("Player "+player +" Wins!");
				return true;
			}
			if ((board[0][j]==board[1][j])&&(board[1][j]==board[2][j])&&(board[1][j]!=' '))
			{
				System.out.println("Player "+player +" Wins!");
				return true;

			}



		}
		return false;
	}
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}